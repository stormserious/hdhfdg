import os

#Modulos
def menu():
    print('1 - Registrar productos. ')
    print('2 - Mostrar el listado de productos. ')
    print('3 - Mostrar productos en Verificar stock desde un intervalo. ')
    print('4 - Sumar stock a los productos. ')
    print('5 - Eliminar productos sin stock. ')
    print('6 - Salir ')
    opcion = int(input('Ingrese una opcion: '))
    while not (opcion >= 1 and opcion <= 6):
        opcion = int(input('Ingrese una opcion: '))
    return opcion

def validarX(x):
    x=int(input('Debe ingresar un valor positivo: ')) 
    while  0>x :
       x = int(input('Debe ingresar un valor poitivo: '))
    return x    

def buscarClave(diccionario,clave):
    datos=diccionario.get(clave,-1)
    return datos

def leerProductos():
    productos =    {90:['Afeitadora', 200, 10],
                    30:['Alcohol', 300, 0],
                    40:['Desinfectante', 550, 4],
                    50:['Cream corporal',330.50, 6]}

    respuesta = 's'
    while respuesta == 's':
        codigo = int(input('Ingrese el codigo: '))
        dato = buscarClave(productos,codigo)

        while dato != -1: 
            codigo = int(input('Ingrese el codigo: '))
            dato = buscarClave(productos,codigo)  

        nombre = input('Ingrese el nombre: ')
        precio = float(input('Ingrese el precio : '))

        if precio<0:
            precio=validarX(precio)
        stock = int(input('Ingrese el stock: '))
        if stock<0:
            stock=validarX(stock)

        productos[codigo] = [nombre,precio,stock]
        print('Se ha agregado el producto.')
        respuesta = input('Desea agregar otro producto?: (s / n)')
    return productos   
    
def mostrar(diccionario):
    print('Listado de Productos')
    for clave, valor in diccionario.items():
        print(clave,valor)

def mostrarDesdeHasta(diccionario):
    d = int(input('Verificar stock desde: '))
    h = int(input('Verificar stock hasta: '))
    while  not d<h:
        d = int(input('Verificar stock desde: '))
        h = int(input('Verificar stock hasta: '))
    for clave, valor in diccionario.items():
        if d<=valor[2]<=h:
            print(clave,valor)

def sumaStock(productos):
    y = int(input('Ingrese el valor de stock menores: '))
    x = int(input('Ingrese cantidad de stock a incrementar: '))
    for clave, valor in productos.items():
        if valor[2]<y:
            valor[2]+=x
    print('Se han incrementado correctamente')
    return productos

def eliminarElemtos(diccionario,codigos):
    for item in codigos:
        del diccionario[item]
    return diccionario

def eliminarStock(productos):
    codigos=[] 
    for clave,valor in productos.items():    
        if valor[2]==0:
            codigos.append(clave)
    print('Se han eliminado productos sin stock´s')
    return eliminarElemtos(productos,codigos)

#Cuerpo del programa

os.system('cls')
opcion = 0
bandera= False

while opcion != 6: 
    opcion = menu() 	
    if opcion == 1: 
        productos = leerProductos()
        bandera=True

    elif opcion == 2 and bandera:
        mostrar(productos)

    elif opcion == 3 and bandera:
        mostrarDesdeHasta(productos)

    elif opcion == 4 and bandera:
        productos = sumaStock(productos)

    elif opcion == 5 and bandera:
	    productos = eliminarStock(productos)

    elif opcion == 6: 
        print('Fin del programa') 

    else:
        print('Para realizar otra opcion, debe realizar la 1 con obligacion') 